## Nginx code deploy 
This is a simple example how a source code could be delivered to production using docker-compose.yml file

## Motivation
1. To pass interview.
2. To improve expirence in DevOps

## Build stage:
Build process is configured using .gitlab-ci.yml
1. I have used prepared nginx image with php: richarvey/nginx-php-fpm:latest
2. All source code located at "src/" directory
3. Gitlab runner in GCP: 35.202.20.141
4. Web server in GCP: 34.66.54.211
5. Image is preparing on runner and uploading to Docker Hub docker.io. <br>
   Two stages of build: <br>
   a. Image is preparing using Dockerfile and tagging with variable $CI_COMMIT_SHA. And then pushing to Docker HUB. <br>
   b. The second stage is triggering based on changes.
      1. If changes is commited on master, we age getting image prepared on step 'a' and tagging image as "latest" and uploading to hub back.
      2. If commit has a TAG (I am using the following tags ex: "0.0.5"), we age getting image prepared on step 'a' and tagging as $CI_COMMIT_TAG
6. Deploy Stage: <br>
Only tagged commit is pushing to server. <br>
   a. I am using 'sshpass' command on runner to move 'docker-compose-prod.yml' to production web server <br>
   b. On next step I am running 'docker-compose' command to run prepared image with TAG.

7. How to deploy a new code. <br>
a. Open http://34.66.54.211 <br>
   You will see "Hello World 18" or something similar "Hello World 'number'" <br>
b. Clone repository: <br>
   git clone https://gitlab.com/yzhigulskiy/nginx.git <br>
c. Make change in src/index.php file <br>
d. # git add *; git commit -m "Describe your changes here"; git push -u origin <br>
   If your changes not broken CI process. You could check it here https://gitlab.com/yzhigulskiy/nginx/pipelines Initialize a deploy by adding a version TAG <br>
e. # git tag -a 0.0.19 -m "version 0.0.19"; git push -u origin --tags <br>
Wait about 1-2 min and check you changes by opening your URL <br>
f. Open http://34.66.54.211 <br>

